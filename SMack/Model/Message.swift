//
//  Message.swift
//  SMack
//
//  Created by Giovanni Pellizzoni on 08/01/19.
//  Copyright © 2019 Giovanni Pellizzoni. All rights reserved.
//

import Foundation


struct Message : Decodable {
    public private(set) var messageBody: String!
    public private(set) var userName: String!
    public private(set) var channelId: String!
    public private(set) var userAvatar: String!
    public private(set) var userAvatarColor: String!
    public private(set) var id: String!
    public private(set) var timeStamp: String!
    
    init(messageBody: String, userName: String, channelId: String, userAvatar: String, userAvatarColor: String, id: String, timeStamp: String) {
        self.messageBody = messageBody
        self.userName = userName
        self.channelId = channelId
        self.userAvatar = userAvatar
        self.userAvatarColor = userAvatarColor
        self.id = id
        self.timeStamp = timeStamp
    }
    
}
