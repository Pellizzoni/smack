//
//  Channel.swift
//  SMack
//
//  Created by Giovanni Pellizzoni on 08/01/19.
//  Copyright © 2019 Giovanni Pellizzoni. All rights reserved.
//

import Foundation

struct Channel : Decodable{
    
    public private(set) var _id: String!
    public private(set) var name: String!
    public private(set) var description: String!
    
    init(channelTitle: String, channelDescription: String, id: String) {
        self.name = channelTitle
        self.description = channelDescription
        self._id = id
    }
    
    //    public private(set) var channelTitle: String!
    //    public private(set) var channelDescription: String!
    //    public private(set) var id: String!
    //
    //    init(){}
    //
    //    init(channelTitle: String, channelDescription: String, id: String) {
    //        self.channelTitle = channelTitle
    //        self.channelDescription = channelDescription
    //        self.id = id
    //    }
}
