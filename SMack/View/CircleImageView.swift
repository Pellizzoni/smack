//
//  CircleImageView.swift
//  SMack
//
//  Created by Giovanni Pellizzoni on 08/01/19.
//  Copyright © 2019 Giovanni Pellizzoni. All rights reserved.
//

import UIKit

@IBDesignable
class CircleImageView: UIImageView {

    override func awakeFromNib() {
        setUpView()
    }

    func setUpView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setUpView()
    }
}
